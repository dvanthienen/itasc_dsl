-- set of all tasks, declared early for convenience
-- -- task level -- --
my_tasks = CompositeTask{
  
-- copy this task template for each task you have:
  Task{
    name = "my_first_task",
    vkc = VKC{type= "iTaSC::my_VKCtype", package="my_task_or_vkc_package",
      o1 = "my_robot.frame", 
      o2 = "my_robot.other_frame",
--    most task/VKC packages provide a default configuration file, at a location similar to:
      config={
        "file://my_package#/cpf/my_VKCname.cpf" ,
--      if sixDof_pff, it is possible to set the chain definition directly:
--      {chain={"TransX","TransY","TransZ","RotX","RotY","RotZ"}}
      },
    },
    cc = CC{
    type="iTaSC::my_CCtype", package="my_task_or_CC_package", prio_num = 1,
--    most task/CC packages provide a default configuration file, at a location similar to:  
      config={
        "file://My_package#/cpf/my_CCname.cpf",
--      if sixDof_pff, it is possible to set the number of constraints nc, the constraint_operator, and control type directly, e.g.:
--        {nc=6,
--        constraint_operator={"equality","equality","equality","equality","equality","equality"},
--        control_type={"pos","pos","pos","pos","pos","pos"}}
    }},
--  most task/CC packages provide a default FSM, at a location similar to:
    fsm = FSM{fsm = "file://my_taskpackage#/scripts/my_first_task_supervisor.lua"},
    connect = {
	  Rewire{src="my_setpoint_generator", tgt="cc", {from="all",to="all"} }
    },
  },
} --end of tasks

-- begin of the actual application (root of the tree of composition in this file)
-- -- application level -- --
return Application {
  dsl_version = '0.1',
  name = 'my_application',
  uri = 'be.kuleuven.mech.robotics.application.my_application',
-- default application FSM and supervisor, will/should work out of the box:
  fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_application_supervisor.lua"},

-- -- setpoint generators -- --
  setpoint_generators = {
--  copy this template of a setpoint generator for each setpoint generator you have
    SetpointGenerator{
      name="my_setpoint_generator",
	 	  type="trajectory_generators::my_setpoint_generator",
	 	  package="my_setpoint_generator_package",
        config = { "file://my_setpoint_generator_package#/cpf/my_sp_config.cpf",
--      in case of the nAxesGeneratorPos, following configuration is  possible:
--      config = {{constraint_value = {0, 0, 0, 0, 0, 0},
--         execution_time = {10, 10, 10, 10, 10, 10},
--         maximum_velocity = {2, 2, 2, 2, 2, 2},
--         maximum_acceleration = {2, 2, 2, 2, 2, 2}
--      }
        },
    },
  },

-- -- drivers -- --
  drivers = {
--  copy this driver template of a driver for each driver you need for a robot (optional), see itasc_pr2 package of the itasc_robots_objects stack for an example:
    Driver{name="my_robot_driver", package="my_robot_driver_package", file="file://my_robot_driver_package#/scripts/my_robot_driver.lua"},
  },
   

-- -- itasc level -- --
  itasc = iTaSC {
    dsl_version = '0.1',
--  use unique names: don't use the same name as the application
    name = 'my_itasc',
    uri = 'be.kuleuven.mech.robotics.itasc.my_itasc',
--  default itasc FSM and supervisor, will/should work out of the box, starting all tasks at once:
    fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_itasc_supervisor.lua"
--  change settings (parts of FSM to use) by adding a configuration file, e.g.:
    --  , config={"file://my_package#/cpf/my_itasc_supervisor.cpf"}
    },

-- -- robots -- --
    robots = {
--    copy this robot template for each robot you have
   	  Robot{
	      name = "my_robot",
	      package = "my_robot_package",
	      type = "iTaSC::my_robot",
          driver = "my_robot_driver",
          config = {"file://my_robot_package#/cpf/my_robot.cpf"}
	    },
    },
     
-- -- objects -- --
    objects = {
--    copy this object template for each object you have
	    Object{
	      name = "my_object",
	      package = "my_object_package",
	      type = "iTaSC::my_object",
	    },
    },

-- -- scene -- --
--   remark: the 'world' frame is implicit at X=0, Y=0, Z=0, and unity rotation
     scene = {
--     copy this scene element template for each robot and object you have
	     SceneElement {
	       robot = "my_robot",
	       location = Frame {
	          M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
	          p = Vector{X=0.0,Y=0.0,Z=0.0}
	       },
	     },

	     SceneElement {
	       object = "my_object",
	       location = Frame {
	         M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
	         p = Vector{X=1.0,Y=0.0,Z=0.0}
	       },
	     },
     },

--   here we add the tasks, defined in the beginning of this file:
     tasks = my_tasks,

-- -- solver -- --
     solver = Solver{ name="Solver", package="wdls_prior_vel_solver", type="iTaSC::WDLSPriorVelSolver" }

  }, --end of iTaSC
} --end of application
