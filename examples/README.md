iTaSC DSL examples
==================
As detailed and presented in the IROS 2013 paper:
"Rapid application development of constrained-based task modelling and execution using Domain Specific Languages", D. Vanthienen et al.

Running example
---------------
- `open_drawer.lua`: the full drawer opening demo: reach, grasp, and pull open
- `simple_open_drawer.lua`: the 'pull the drawer open' part of the drawer opening demo

Adaptations of running example
------------------------------
- `simple_open_drawer_youbot.lua`: PR2 replaced by the KUKA youBot
- `simple_open_drawer_cylindricalvkc.lua`: replacing the VKC model to cylindrical coordinates
- `open_drawer_fsm.lua`: replacing the composite FSM resulting in different behavior
- `simple_open_drawer_leftgripper.lua`: open the drawer with the left gripper i.s.o. the right gripper
- `simple_open_drawer_otherdrawer.lua`: open another drawer
- `simple_open_drawer_rim.lua`: grasp the rim of the drawer i.s.o. the handle
- `simple_open_drawer_wprio.lua`: change of weights and priorities of the tasks

Elaborate examples
------------------
- `comanipulation_demo.lua`: human-robot comanipulation demo
- `itasc_erf2012_demo_model.lua`: laser tracing demo with a KUKA youBot
- `itasc_erf2012_demo_pr2_model.lua`: laser tracing demo with a PR2
